$(document).ready(function () {
    $('#btn-1').addClass('btn-click');

    /* Code for navbar */
    $('.nav-link').on('click', function () {
        $('.navbar-collapse').collapse('hide');

    });


    /* Code for merchandise */
    $(".post-wrapper").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nextArrow: $(".next"),
        prevArrow: $(".prev"),
        responsive: [
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 360,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            }
        ]
    });

    /* Code for testimonials */
    $('.testimonials-row').slick({
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        touchMove: true,
        prevArrow: ".arrow-prev",
        nextArrow: ".arrow-next",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },


            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }

            }
        ]
    });

    /* Code for portfolio */
    $(document).on('click', '.button-group button', function (e) {
        $(".btn-click").not($(this).addClass('btn-click')).removeClass('btn-click');

        var btnId = this.id;

        if (btnId == "btn-1") {
            $('.all').hide();
            $(".img-thumbnail:hidden").slice(0, 6).fadeIn(1000);
            $('#btn-portfolio').fadeIn(1500);
        } else if (btnId == "btn-2") {
            $('.all').hide();
            $('.category-3').fadeIn(1000);
            $('#btn-portfolio').fadeOut(1500);
        } else if (btnId == "btn-3") {
            $('.all').hide();
            $('.category-2').fadeIn(1000);
            $('#btn-portfolio').fadeOut(1500);
        } else if (btnId == "btn-4") {
            $('.all').hide();
            $('.category-4').fadeIn(1000);
            $('#btn-portfolio').fadeOut(1500);
        } else if (btnId == "btn-5") {
            $('.all').hide();
            $('.category-1').fadeIn(1000);
            $('#btn-portfolio').fadeOut(1500);
        } else if (btnId == "btn-6") {
            $('.all').hide();
            $('.category-5').fadeIn(1000);
            $('#btn-portfolio').fadeOut(1500);
        }

    });

    let nav = document.querySelector('nav');

    window.addEventListener('scroll', function () {
        if (window.pageYOffset > 100) {
            nav.classList.add('bg-dark', 'shadow');
        } else {
            nav.classList.remove('bg-dark', 'shadow');
        }
    });


    document.querySelector('.first-button').addEventListener('click', function () {
        document.querySelector('.animated-icon1').classList.toggle('open');
    });
    document.querySelector('.navbar-nav').addEventListener('click', function () {
        document.querySelector('.animated-icon1').classList.toggle('open');
    });


});


var btns =
    $("#navigation .navbar-nav .nav-link");

for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click",
        function () {
            var current = document
                .getElementsByClassName(".active");

            current[0].className = current[0]
                .className.replace(" .active", "");

            this.className += " .active";
        });
}

/* Code for changing active
link on Scrolling */
$(window).scroll(function () {
    var distance = $(window).scrollTop();
    $('.page-section').each(function (i) {

        if ($(this).position().top
            <= distance + 150) {

            $('.navbar-nav a.active')
                .removeClass('active');

            $('.navbar-nav a').eq(i)
                .addClass('active');
        }
    });
}).scroll();


/* Code for smooth scroll */
$(document).on('click', 'a[href^="#"]', function (e) {
    e.preventDefault();
    $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 0, 'linear');
})

// code for load more button

$(function () {
    $('.img-thumbnail').hide();
    $(".img-thumbnail:hidden").slice(0, 6).fadeIn(500);
    $("#btn-portfolio").click(function (e) {
        e.preventDefault();
        $(".img-thumbnail:hidden").slice(0, 6).slideDown(500);
        if ($(".img-thumbnail:hidden").length == 0) {
            $('#btn-portfolio').fadeOut(1500);
        }
    });
});

$(document).ready(
    function () {
        let chatBox = document.getElementById('fb-customer-chat');
        chatBox.setAttribute("page_id", "103994424641406");
        chatBox.setAttribute("attribution", "biz_inbox");

        window.fbAsyncInit = function () {
            FB.init({
                xfbml: true,
                version: 'v11.0'
            });
        };

        (function (d, s, id) {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.com/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
);
